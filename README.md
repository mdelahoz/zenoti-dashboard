# Assigment
The following are two screenshots of the app we want you to invest time into. You can use publicly available images or material design to replicate the screens. A few actions you can invest time into:

![Dashboard](./screenshots/Dashboard__1_.png)
![Homepage](screenshots/Homepage__1_.png)

- Burger menu opening and closing
- Changing the timeline from 1 week to 1 year
- tapping on the metrics opens the graph for that metric

You are free to make assumptions that you feel will enhance the experience for the ​users.

### Solution Stack

- MeteorJs
- ReactJS
- MaterialUI

 1. cd to the project folder `cd /zenoti-dashboard`
 2. run meteor from command line `meteor`
