import React from 'react';
import Homepage from './Homepage.jsx';

const App = () => (
  <div>
    <Homepage />
  </div>
);

export default App;
