import React, {Component} from 'react';
import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import NumberFormat from 'react-number-format';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const ProductsContainerWrapper = styled.div`
  background-color: orange;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
`;

const ProductHeader = styled.div`
  background-color: orange;
  display: flex;
  flex-direction: row;
  // align-items: center;
  justify-content: space-between;
  font-size: calc(10px + 1vmin);;
  color: white;
`;

const ProductBody = styled.div`
  background-color: orange;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 1vmin);;
  color: white;
`;


class ProductsContainer extends Component {
  render() {
    // 'Revenue', 'Utilization', 'Feedback', 'Guests'
    const productsJSON = [
      {_id: 1, title: 'PRODUCT', revenue: 778895, period: "May 15 - May 21", prev_revenue: 807520, prev_period: "May 18 - May 14" },
      {_id: 2, title: 'PRODUCT', revenue: 778895, period: "May 15 - May 21", prev_revenue: 807520, prev_period: "May 18 - May 14" },
      {_id: 3, title: 'PRODUCT', revenue: 778895, period: "May 15 - May 21", prev_revenue: 807520, prev_period: "May 18 - May 14" },
      {_id: 4, title: 'PRODUCT', revenue: 778895, period: "May 15 - May 21", prev_revenue: 807520, prev_period: "May 18 - May 14" },
      {_id: 5, title: 'PRODUCT', revenue: 778895, period: "May 15 - May 21", prev_revenue: 807520, prev_period: "May 18 - May 14" },
      {_id: 6, title: 'PRODUCT', revenue: 778895, period: "May 15 - May 21", prev_revenue: 807520, prev_period: "May 18 - May 14" },
    ];

    const products  = productsJSON.map(
      product => this.makeProduct(product),
    );

    return (
      <Grid>
        {products}
      </Grid>
    );
  }

  makeProduct(product) {
    // const classes = useStyles();
    // const bull = <span className={classes.bullet}>•</span>;
    return (
      // <div key={product._id}>
      //   <ProductHeader>{product.title} </ProductHeader>
      //   <ProductBody>
      //     {product.revenue}
      //     {product.period}
      //     {product.prev_revenue}
      //     {product.prev_period}
      //   </ProductBody>
      // </div>
      <Card key={product._id} >
        <CardContent>
          <Typography variant="h5" component="h4">
            {product.title}
          </Typography>
          <Typography variant="body2" component="p">
            <NumberFormat value={product.revenue} displayType={'text'} thousandSeparator={true} prefix={'$'} />
            <br />
            {product.period}
          </Typography>
          <Typography variant="body2" component="p">
            <NumberFormat value={product.prev_revenue} displayType={'text'} thousandSeparator={true} prefix={'$'} />
            <br />
            {product.prev_period}
          </Typography>
        </CardContent>
        {/*<CardActions>*/}
        {/*  <Button size="small">Learn More</Button>*/}
        {/*</CardActions>*/}
      </Card>
    )
  }
}

export default ProductsContainer;
