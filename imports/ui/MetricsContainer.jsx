import React, { Component } from 'react';
// import Button from 'react-bootstrap/Button';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import TimeLine from './TimeLine'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(10),
    },
  },
}));

const MetricsContainerWrapper = styled.div`
  background-color: gray;
  display: flex;
  flex-direction: row;
  align-items: stretch;
  align-content: flex-start;
  justify-content: space-evenly;
  font-size: calc(10px + 1vmin);;
  color: white;
`;

const MetricWrapper = styled.ul`
  list-style-type: none;
`;

const MetricItemWrapper = styled.li`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: space-between;
`;

class MetricsContainer extends Component {

  constructor() {
    super();
    this.state = {
      metric: '',
    };
  }

  handleClick = (metric) => {
    console.log('set metric here');
    console.log(metric);
    this.setState({
      metric,
    })
    this.props.onSelectMetric({metric});
  };

  render() {
    // 'Revenue', 'Utilization', 'Feedback', 'Guests'
    const metricsArr = [
      {_id: 1, title: 'Revenue', value1: '178.895', value2: '107.520' },
      {_id: 2, title: 'Utilization', value1: '67%', value2: '54%' },
      {_id: 3, title: 'FeedBack', value1: '4.7', value2: '4.9' },
      {_id: 4, title: 'Guests', value1: '718', value2: '718' },
    ];
    const metrics  = metricsArr.map(
      metric => this.makeMetric(metric),
    );

    return (
      // <MetricsContainerWrapper>
      //
      // </MetricsContainerWrapper>
      <ButtonGroup size="large" color="primary" aria-label="large outlined primary button group">
        {metrics}
      </ButtonGroup>
    );
  }

  makeMetric(metric) {
    return (
      <Button key={metric._id} color="primary" onClick={this.handleClick.bind(this, metric._id)}>
        <MetricWrapper>
          <MetricItemWrapper>{metric.title}</MetricItemWrapper>
          <MetricItemWrapper>{metric.value1}</MetricItemWrapper>
          <MetricItemWrapper>{metric.value2}</MetricItemWrapper>
        </MetricWrapper>
      </Button>
    );
  }
}

export default MetricsContainer;
