import React, { Component } from 'react';
import Dashboard from './Dashboard.jsx';
import MetricsContainer from "./MetricsContainer";
import ProductsContainer from "./ProductsContainer";

export default class Homepage extends Component {
  state = {
    counter: 0,
  }

  render() {
    return (
      <div>
        <Dashboard />
        {/*<MetricsContainer />*/}
        {/*<ProductsContainer/>*/}
      </div>
    );
  }
}
