import React, { Component } from 'react';
// import Button from 'react-bootstrap/Button';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CalendarToday from '@material-ui/icons/CalendarToday';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    '& > *': {
      margin: theme.spacing(10),
    },
  },
}));

class TimeLine extends Component {
  constructor() {
    super();
    this.state = {
      frame: '',
    }
  }

  handleClick = (frame) => {
    console.log('set time frame here');
    console.log(frame);
    this.setState({
      frame,
    })
    this.props.onSelectTime({frame});
  };

  render() {
    // 'Revenue', 'Utilization', 'Feedback', 'Guests'
    const metricsArr = [
      {_id: 1, title: '1w' },
      {_id: 2, title: '4w' },
      {_id: 3, title: '1y' },
      {_id: 1, title: 'Mtd' },
      {_id: 2, title: 'Qtd' },
      {_id: 3, title: 'Ytd' },
    ];
    const { selectedMetric } = this.props;

    const metrics  = metricsArr.map(
      metric => this.makeMetric(metric),
    );

    console.log('time line component selected metric', selectedMetric);
    return (
      <ButtonGroup size="large" color="primary" aria-label="large outlined primary button group">
        {metrics}
        <IconButton
          edge="start"
          color="inherit"
          aria-label="open drawer"
          onClick={()=>console.log('this')}
        >
          <CalendarToday />
        </IconButton>
      </ButtonGroup>
    );
  }

  makeMetric(metric) {
    const { selectedMetric } = this.props;
    const buttonDisabled = selectedMetric === '';
    return (
      <Button key={metric._id} color="primary" disabled={buttonDisabled} onClick={this.handleClick.bind(this, metric._id)}>
        {metric.title}
      </Button>
    );
  }
}

export default TimeLine;
